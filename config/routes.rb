Rails.application.routes.draw do
	# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
	namespace :v1 do
		resources :employees do
			get :single_employee_details
			get :full_employee_details, on: :collection
		end

		#get 'employees/full_employee_details', to: 'employees#full_employee_details'
		#get 'employees/single_employee_details/:employee_id', to: 'employees#single_employee_details'

		resources :projects do
			get :full_project_details, on: :collection
		end

		put 'request_phone_number', to: 'phone#request_phone_number'

		#post 'save_phone_number', to: 'api#save_phone_number'
		#patch 'save_phone_number/:phone_number_id', to: 'api#update_phone_number'
		#delete 'save_phone_number/:phone_number_id', to: 'api#delete_phone_number'
		#get 'projct/full_project_details', to: 'projects#full_project_details'
	end
end
