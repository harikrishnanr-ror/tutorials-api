class V1::ProjectsController < ApplicationController

	def index
		#@projects = Project.joins(:employee).select('projects.*,employees.id as empid,employees.first_name as first_name, employees.email_id as email_id')
		@projects = Project.all
		render json:@projects, status: :ok
	end

	def full_project_details
		projects = Project.all
		project_array = project_array1 =[]

		projects.each do |prjct|
			prjct_emps = Employee.where(id: prjct.employee_id)
			project_array1 = {
				"id" => prjct.id,
				"project_name" => prjct.project_name,
				"project_description" => prjct.project_description,
				"employees" => prjct_emps
			}
			project_array << project_array1
		end
		render json: { status: 'success', data: project_array }, status: :ok
	end

	def create
		@projects = Project.new(project_params)
		if (@projects.valid?)
			if (@projects.save)
				render json: { status: 'success', message: 'Project Added Successfully', data: @projects}, status: :ok
			else
				render json: { status:'error', message: 'Something went wrong', data: @projects.errors}, status: :unprocessable_entity
			end
		else
			render json: {status: 'error', message: 'All fields are required', data: @projects.errors}, status: :unprocessable_entity
		end
	end

	def update
		if(!Project.where(id: params[:id]).empty?)
			@projects = Project.find(params[:id])
			if(@projects.valid?)
				if (@projects.update_attributes(project_params))
					render json: { status: 'success', message: 'Project updated Successfully', data: @projects }, status: :ok
				else
					render json: { status: 'error', message: 'Something went wrong', data: @projects.errors}, status: :unprocessable_entity	
				end
			else
				render json: {status: 'error', message: 'All field are required', data: @projects.errors}, status: :unprocessable_entity
			end
		else
			render json: {status: 'error', message: 'Project not found'}, status: :unprocessable_entity
		end
	end

	def show
		if !Project.where(id: params[:id]).empty?
			@projects = Project.find(params[:id])
			render json: { status:'success', data: @projects}, status: :ok
		else
			render json: {status: 'error', message: 'Project not found'}, status: :unprocessable_entity
		end
	end

	def destroy
		if(!Project.where(id: params[:id])).empty?
			@projects = Project.find(params[:id])
			if(@projects.destroy)
				render json: { status: 'success', message: 'Project deleted Successfully', data: @projects}, status: :ok
			else
				render json: { status: 'error', message: 'Something went wrong', data: @projects.errors}, status: :unprocessable_entity
			end
		else
			render json: { status: 'error', message: 'Something went wrong', data: @projects.errors }, status: :unprocessable_entity
		end
	end

	private
		def project_params
			params.require(:project).permit(
				:project_name,
				:project_description,
				:status,
				:employee_id
			);
		end
end