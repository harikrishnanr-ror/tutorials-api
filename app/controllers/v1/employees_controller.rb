class V1::EmployeesController < ApplicationController

	def index
		@employee = Employee.all
		render json:@employee, status: :ok
	end

	def full_employee_details
		#@employee = Employee.all.includes(:project).select('projects.project_name').references('projects')
		@employee = Employee.all
		employee_array = employee_array1 = [];
		@employee.each do |emp|
			emp_prjcts = Project.where(employee_id: emp.id, status: 1)
			employee_array1 = { 
						'id' => emp.id, 
						'first_name' => emp.first_name,
						'email_id' => emp.email_id,
						'password' => emp.password,
						'age' => emp.age,
						'gender' => emp.gender,
						'phone_number' => emp.phone_number,
						'projects' => emp_prjcts }
			employee_array << employee_array1
			#Rails.logger.debug("EmployeeID #{).inspect}");
		end
		render json:employee_array, status: :ok
	end

	def create
		@employee = Employee.new(employee_params)
		# @employee.attributes(params.require(:employee)).permit(:email_id)
		# @employee.inserting_context = :interface

		if @employee.valid?
			if @employee.save
				render json: { status: 'success', message: 'Employee Saved', data: @employee}, status: :ok
			else
				render json: { status: 'error', message: 'Employee not saved', data: @employee.errors }, status: :unprocessable_entity
			end
		else
			render json: { status: 'error', message: 'All Fields are Required', data: @employee.errors}, status: :unprocessable_entity
		end
	end

	def show
		if !Employee.where(id: params[:id]).empty?
			#@employee = Employee.left_joins(:project).select('employees.*,projects.*').where('employees.id = "'+params[:id]+'"')
			@employee = Employee.find(params[:id])
			render json: { status:'success', data: @employee}, status: :ok
		end
	end

	def update
		if !Employee.where(id: params[:id]).empty?
			@employee = Employee.find(params[:id]);
			if @employee.update_attributes(employee_params)
				render json: { status: 'success', message: 'Employee updated successfully', data: @employee}, status: :ok
			else
				render json: { status: 'error', message: 'Employee not updated', data: @employee.errors}, status: :unprocessable_entity
			end
		else
			render json: {status: 'error', message: 'Employee not found'}, status: :unprocessable_entity
		end
	end

	def destroy
		if !Employee.where(id: params[:id]).empty?
			@employee = Employee.find(params[:id])
			@employee.destroy
			render json: { status: 'success', message: 'Employee Deleted'}, status: :ok
		else
			render json: { status: 'error', message: 'Data not available'}, status: :unprocessable_entity
		end
	end

	def single_employee_details
		if !Employee.where(id: params[:employee_id]).empty?
			#page = params[:page] || 1
			employee = Employee.all.includes(:project).where('employees.id = ?',params[:employee_id]);
			full_projects = []
			if(employee.present?)
				employee.each do |emp|
					full_project = emp.project
					#puts emp
					# next unless full_project.present?
					# 	full_project = full_project
						#Rails.logger.debug("EmployeeID #{full_project.inspect}");
						full_projects << full_project
				end
			end
			# @project = Project.where(employee_id: params[:employee_id]);
			# @final_array = {
			# 	"employee" =>@employee,
			# 	"project"=>@project
			# }
			#Rails.logger.debug("EmployeeID #{employee.inspect}");
			render json: { status: 'success', message: 'Employee Data', data: full_projects}, status: :ok
		else
			render json: { status: 'error', message: 'Employee not available'}, status: :unprocessable_entity
		end
	end

	private
		def employee_params
			params.require(:employee).permit(
				:first_name, 
				:email_id, 
				:password, 
				:age, 
				:gender, 
				:phone_number)
		end
end
