class V1::PhoneController < ApplicationController
	def request_phone_number
		if(params[:employee_id])
			if(params[:mobile_number])
				phone = PhoneNumber.new(phone_params)
				if(phone.save)
					render json: { status: 'success', message: 'Phone number added successfully', data: phone}, status: :ok
				else
					phone_num = generate_phone_number(params[:employee_id])
					render json: { status: 'success', message: 'Here is your number, old number already available', data: phone_num}, status: :ok
				end
			else
				phone_num = generate_phone_number(params[:employee_id])
				render json: { status: 'success', message: 'Here is your number', data: phone_num}, status: :ok
			end
		else
			render json: {status: "error", message: "Employee Id is null"}, status: :unprocessable_entity
		end
	end

	private
		def generate_phone_number(emp_id)
			phone = '';
			loop do
				phone1 = (rand 111..999).to_s
				phone2 = (rand 111..999).to_s
				phone3 = (rand 1111..9999).to_s
				finalNum = phone1+'-'+phone2+'-'+phone3
				phone = PhoneNumber.new(:mobile_number => finalNum, :employee_id=> emp_id)
				break if (phone.save!)
			end
			return phone
		end

		def phone_params
			params.permit(
				:mobile_number,
				:employee_id
				)
		end
end
