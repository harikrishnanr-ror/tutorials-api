class Employee < ApplicationRecord
	validates_presence_of :first_name, :password, :age, :gender, :phone_number

	validates_uniqueness_of :first_name

	validates_presence_of :email_id
	validates_uniqueness_of :email_id
	validates_format_of :email_id, with: /\A[^@\s]+@([^@\s]+\.)+[^@\W]+\z/

	validates :phone_number,:length => { :minimum=>10, :maximum=>15 }
	validates_uniqueness_of :phone_number
	validates_format_of :phone_number, :with => /\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}/ , :message => "Phone numbers must be in xxx-xxx-xxxx format."

	has_many :projects
	has_many :phone_numbers
end