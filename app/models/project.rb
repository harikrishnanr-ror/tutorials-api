class Project < ApplicationRecord
	belongs_to :employee

	validates_presence_of :project_name, :project_description, :status, :employee_id
end