class PhoneNumber < ApplicationRecord
	belongs_to :employee
	validates_presence_of :employee_id
	validates :mobile_number, :length => { :minimum => 12, :maximum=> 12}
	validates_uniqueness_of :mobile_number
	validates_format_of :mobile_number, :with => /\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}/ , :message => 'XXX-XXX-XXXX'
end