class CreateEmployees < ActiveRecord::Migration[6.0]
	def change
		create_table :employees do |t|
		t.string :first_name
		t.string :email_id, unique: true
		t.string :password
		t.integer :age
		t.string :gender
		t.string :phone_number
		t.timestamps
		end
	end
end
