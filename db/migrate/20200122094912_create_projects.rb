class CreateProjects < ActiveRecord::Migration[6.0]
	def change
		create_table :projects do |t|
			t.string :project_name
			t.string :project_description
			t.boolean :status
			t.belongs_to :employee, foreign_key: true
			t.timestamps
		end
	end
end
