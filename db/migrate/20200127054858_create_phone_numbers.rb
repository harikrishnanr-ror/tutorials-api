class CreatePhoneNumbers < ActiveRecord::Migration[6.0]
	def change
    	create_table :phone_numbers do |t|
    	t.text :mobile_number, unique: true
      	t.timestamps
    	end

    	#add_index :phone_numbers, :phone_number, unique: true
  	end
end
